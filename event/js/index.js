const button = document.querySelector('button');
const remove_button = document.querySelector('.remove');

// 이벤트 동작 설정
handler = (event) => {
    console.log("!!!");
}

// 등록한 이벤트 삭제
remove_handler = (event) => {
    button.removeEventListener('click',handler)
}

// 이벤트 종류, 이벤트 핸들러 함수
button.addEventListener('click', handler)
remove_button.addEventListener('click', remove_handler)