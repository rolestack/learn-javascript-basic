// 인자(Arguments) - 함수의 입력 값

function bok(main){
    console.log(`${main} 뽀끔밥`);
}
bok('새우');
bok('제육');

console.log("---");

function sum(a, b){
    console.log(a+b);
}
sum(1,2);
sum(1); // 값을 할당하지 않으면 undefined 값을 가짐
sum(1, undefined); // 도출된 NaN은 숫자가 아니라는 뜻

// 매개변수(Parameter) - 함수의 입력 변수
function a(){
    const b = 10;
    console.log(b);
}
a();
//console.log(b); // 함수 내 지역변수라 호출불가

console.log("---");

// default paramter
function sum(a, b = 0){
    console.log(a+b);
    console.log(arguments[0]);
    console.log(arguments[1]);
    console.log(arguments[2]);
}
sum(10);
sum(a=10,b=20);
sum(10,20,30);

console.log("---");

function print_again(a, b, ...rest){
    console.log(a);
    console.log(b);
    console.log(rest);
}
print_again(1,2,3,4,5,6,7); // rest은 arr로 입력됨

console.log("---");

function return_sum(a,b){
    return a+b;
}
const sum_test = return_sum(1,2);
console.log(sum_test);
console.log(return_sum(1,3));