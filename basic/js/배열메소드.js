const arr = [1, 2, 3, 4, 5];

// 배열에 끝부분에 데이터 추가
arr.push(6);
arr.push(7,8);
console.log(arr);

// 배열의 끝부분의 데이터를 뽑아내기(삭제포함)
console.log(arr.pop());
console.log(arr);

// 배열 앞쪽에 데이터 추가
arr.unshift(10, 20, 30);
console.log(arr);

// 배열의 앞부분의 데이터를 뽑아내기(삭제포함)
console.log(arr.shift());
console.log(arr);

// 각각의 요소들의 인자로 주어진 함수를 실행
const print = (number, index) => {
    console.log(`${index} 위치의 요소 : ${number}`);
}
arr.forEach(print);

arr.forEach((number, index) => {
    console.log(`${index} 위치의 요소 : ${number}`);
}); 

// forEach 메소드는 원본 배열의 값을 변경하지도 않고 리턴하지도 않음
const arrEach = arr.forEach((number, index) => number + 1);
console.log(arrEach);

// forEach와 유사하지만 반환값을 가지는 메소드
const arrMap = arr.map((number, index) => number + 1);
console.log(arrMap);

// 배열요소 검색
const arrSearch = ['hello', 'world'];
console.log(arrSearch.includes('hello')); // 있으면 true 반환
console.log(arrSearch.includes('he')); // 없으면 false 반환

// 조건 검색(조건을 만족하는 첫번째 요소만 출력)
const arr_find = [1, 2, 3, 4, 5];
console.log(arr_find.find((number) => number > 3)); // number는 각각의 실행요소를 의미하고 이게 3보다 크면 첫번째 요소를 출력

// 조건 인덱스 검색(조건을 만족하는 첫번째 요소만 출력)
console.log(arr_find.findIndex((number) => number > 3)); // 없으면 -1을 리턴

// 배열을 지정한 값으로 채워줌(원본 배열 수정)
const arr_fill = [1, 2, 3, 4, 5];
arr_fill.fill(10);
console.log(arr_fill);

// 지정한 위치의 인덱스부터 지정한 값으로 채워줌(원본 배열 수정)
arr_fill.fill(100, 1, 3); // 1번 인덱스부터 3번 인덱스까지 수정
console.log(arr_fill);

// 배열의 복사본을 조각내서 새로운 복사본으로 반환(원본 수정 안함)
const arr_slice = [1, 2, 3, 4, 5];
console.log(arr.slice(2, 4));

// 배열의 복사본을 잘라내거나 잘라낸뒤 데이터를 채위넣음(원본 배열 수정)
const arr_splice = [1, 2, 3, 4, 5];
// arr_splice.splice(2);
// arr_splice.splice(2,1); // 2번 인덱스부터 1개의 요소만 제거
arr_splice.splice(2,1,999); // 제거하고 추가
console.log(arr_splice);

// 배열의 모든 요소를 이어붙여서 하나의 문자열로 반환
const arr_join = ["반갑습니다.", "최민식입니다."];
console.log(arr_join.join()); // 기본적으로 쉼표가 들어감
console.log(arr_join.join(" ")); // 커스텀 가능

// 인자로 주어진 배열이나 값을 기존 배열에 합쳐서 새로운 배열로 반환
const arr_str = ["경주최씨", "충렬공파"];
const arr_num = [35,39];
console.log(arr_str.concat(arr_num));
console.log(arr_str.concat("aaa", "bbb"));

// forEach와 map과 유사함, 조건에 맞는 요소를 새로운 배열로 반환
const arr_filter = [1, 2, 3, 4, 5];
console.log(arr_filter.filter((number) => number > 3));

// 배열의 요소를 순차적으로 순회하면서 리듀서 함수를 실행하고 하나의 결과값을 반환
// 필수인자로 리듀서라는 이름의 함수가 요구가 됨
const arr_reduce = [1, 2, 3, 4, 5];
const reducer = (acc, value) => acc + value;
console.log(arr_reduce.reduce(reducer))
console.log(arr_reduce.reduce(reducer, 10)); // 누산기(accumulator)의 초기값을 정할 수 있음