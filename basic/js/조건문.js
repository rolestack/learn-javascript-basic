const a = 10;
const b = 20;
const c = 20;

// if else
if ( a > b ){
    console.log("else / true"); // true
} else { 
    console.log("else / false"); // false
}

// else if
if ( a > b ) {
    console.log("else if / a > b")
} else if (b !== c) {
    console.log("else if / b !== c")
} else if (b === c) {
    console.log('else if / b === c')
} else {
    console.log("else if / else")
}

// switch
const number_3 = 20;

switch (number_3) {
    case 1:
        console.log(number_3 + "is 1");
        break; // case 와 break는 거의 항상 쌍으로 사용해 준다.
        // break를 안넣으면 아래 코드까지 전부 실행함
    case 10:
        console.log(number_3 + "is 10");
        break;
    default: // case가 없을 경우 실행
        console.log("there is no case");
}

// 비교
const number = 10;
if (number % 2 == 0){
    console.log("짝수");
} else {
    console.log("홀수");
}

switch (number % 2){
    case 0:
        console.log("짝수");
        break;
    case 1:
        console.logs("홀수");
        break;
}