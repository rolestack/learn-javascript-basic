//bigint
const number = 123n;
console.log(typeof number);

// string
const name = "박경민";
console.log(typeof name);

// number
const age = 20;
const job = "developer";

// string concatenation
const msg = "i am "+ job + " and " + age + " years old";
console.log(msg);

// backtick
const msg2 = `I am ${job} and ${age} years old`;
console.log(msg2);

// boolean
const isTure = 10 < 20;
console.log(typeof isTure);
console.log(isTure);

// undefined
let hello;
console.log(hello);

//////////////////////////

// array
const arr = [1, 2, 3, 4, 5];
console.log(arr[0]);

const arr_2 = [1, 2, 3, 4, 5, {name: 'pkm'}];
console.log(arr_2)

// object
const obj = {
    name: 'pkm',
    job: 'developer',
    age: 26
}
console.log(obj);
console.log(obj['name']);
console.log(obj.name);

const obj_arr = {
    arr: [1,2,3],
    something: {
        name: "pkm"
    }
}

console.log(obj_arr)