// 배열 또는 객체를 분해하여 개별적인 변수에 담는 표현식

// 배열(Array) 구조분해
let arr = [1, 2, 3, 4, 5];
let [first, second, thrid] = arr;
console.log(first, second, thrid);

// 객체(Object) 구조분해
let obj = {
    x: 10,
    y: 20,
}
let {x, y} = obj;
console.log(x,y)

// 변경할 이름을 지정할 수 있음
obj = {
    x: 10,
    y: 20,
}
let {x: hello, y: world} = obj;
console.log(hello, world);


// 좀더 복잡한 형태의 객체를 가정
obj = {
    one: {
        two: {
            three: 'Bingo',
        }
    }
}
let bingo = obj.one.two.three;
console.log(bingo);

const {
    one: {
        two: { three: key }
    }
} = obj;
console.log(key);

// 함수에서의 구조분해
obj = {
    x: 10,
    y: 20
};

function sum(obj){
    return obj.x + obj.y; // 어떤 프로퍼티가 있는지 정확히 그 키 값을 알때, 점 표기법을 사용하여 접근
}
console.log(sum(obj))

function sum_simple({x: hello, y: world}){
    return hello + world;
};
console.log(sum_simple(obj));


// 활용 예제 1
let a = 123;
let b = 456;
let temp = a;
console.log(a, b);

a = b;
b = temp;
console.log(a,b);

[a, b] = [b,a];
console.log(a,b);


// 활용 예제 2
const [aa = 1, bb = 20] = [10];
console.log(aa, bb);

// 활용 예제 3
const arr_ex = [1, 2, 3, 4, 5];
const [one,,three,,five] = arr_ex;
console.log(one,three,five);

// 활용 예제 4
const arr_ex2 = [1, 2, 3, 4, 5];
const [once, twice, ...others] = arr_ex2;
console.log(once, twice, others);