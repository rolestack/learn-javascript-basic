// 증감 연산자
let number = 10;
number ++;
console.log(number);

// 비교 연산자
const a = 10;
const b = 20;
console.log(a !== b);

// 논리 연산자( &&, ||, ! )
const c = 2 < 3;
const d = 30 > 50;
console.log(c && d); // and
console.log(c || d); // or
console.log(!c) // not

// 삼항 연산자
// 조건 ? 참일때 실행 : 거짓일 때 실행
console.log(2 > 3 ? "참" : "거짓");

// 널리쉬(??)
// 여러 개의 피연산자 중 값이 확정되어 있는 변수를 찾음
const aa = undefined;
const bb = null;
const cc = "pkm";
console.log(aa ?? bb ?? cc); // 왼쪽에서 오른쪽으로 확인


// 비트 연산자 ( & | ~ ^ << >>)
// 임마는 잘 안쓴대

// 복합 대입 연산자(+= -= *= /= %= **=)
let number_2 = 10;
number_2 += 2; // number_2 = number_2 + 2;
number_2 *= 2; // number_2 = number_2 * 2;

// 전개 연산자
// 반복이 가능한 객체에 적용할수있음(arr, obj)
const num_arr = [1, 2, 3];
console.log(...num_arr);
console.log(num_arr);

const num_arr_2 = [4, 5, 6,];
const new_num = [...num_arr, ...num_arr_2]
console.log(...new_num);
console.log(new_num)