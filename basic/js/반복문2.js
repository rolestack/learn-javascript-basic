// for of
// 반복이 가능한 객체, 배열이나 문자열에서 사용할 수 있는 종류
const arr = [1, 2, 3];
for (const i of arr) {
    // arr의 내부 값을 하나하나 가져와 i로 사용
    console.log(i);
}

console.log("---");

const arr2 = [1, 2, 3, [4, 5]];
for (const i of arr2){
    console.log(i)
}

console.log("---while");

// while
// 선조건확인 후실행
let i = 0;
while (i < 10) {
    console.log(i++);
}

console.log("---do_while");

// do while
// 선실행 후조건확인
i = 0;

do {
    console.log(i++);
} while (i < 10);