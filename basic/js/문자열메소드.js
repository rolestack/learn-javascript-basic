// 대소문자
const str = "Hello World";
console.log(str.toLowerCase()); // 소문자로 모두 변경
console.log(str.toUpperCase()); // 대문자로 모두 변경

// 공백 제거
const str_1 = "      hahaha       "
console.log(str_1.trimStart());
console.log(str_1.trimEnd());
console.log(str_1.trimStart().trimEnd());

// 반복 출력
const str_2 = "Hello";
console.log(str_2.repeat(3));
console.log(str_2);

// 패딩 스타트
console.log(str_2.padStart(10, "_"));
console.log(str_2.padEnd(10, "_"));

// 문자열 위치 찾기
const str_3 = "안녕하세요. 최민식입니다.";
console.log(str_3.indexOf('최민식')); // 찾지못하면 -1 반환

// 키워드 일치여부
console.log(str_3.includes("최민식")); // 못찾으면 false 반환

// 특정 문자로 시작하는 지 끝나는지 여부
console.log(str_3.startsWith('안녕'));
console.log(str_3.endsWith('안녕'));
console.log(str_3.endsWith('최민식입니다.'));
console.log(str_3.endsWith('입니다.'));

// 기존 문자열을 새로운 문자열로 반환(맨처음 한개만)
console.log(str_3.replace('최민식', '최형배'));

// 기존 문자열을 새로운 문자열로 반환(모두다)
const str_4 = "니 내 누군지 아나? 내가 임마 느그 서장이랑 어저께도 어! 같이밥먹고 어!";
console.log(str_4.replaceAll('어', '또잉'));

// 부분 출력
console.log(str_3.substring(0, 5)); // 시작 인덱스와 종료 인덱스 -1까지 출력

// 문자열을 특정 규칙에 따라 배열로 분리
const str_5 = "월요일, 화요일, 수요일, 목요일, 금요일, 토요일, 일요일";
console.log(str_5.split(", "));

// 문자열 자르기
const str_6 = "그... 혹시 어데 최씹니까?"
console.log(str_6.slice(0,4)); // 시작과 끝
console.log(str_6.slice(5)); // 시작 위치
console.log(str_6.slice(-8)); // 끝에서 부터 자르기