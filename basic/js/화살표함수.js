// 함수의 표현식
// function sum(a,b){
//     console.log(a+b);
// }

const sum = function (a,b) {
    console.log(a+b);
}

sum(1,2);
console.log(sum(1,2)); // 리턴값이 없어서 함수가 실행되지만 리턴값이 없어 undefined를 반환

console.log("---");

// function을 제거하고 단순한 표현식으로 표시함
const sum_again = (a,b) => a+b; // 중괄호 없이 한줄로 코드를 작성하면 자동으로 return이 지정됨
const sum_again_2 = (a,b) => { // 동일하게 동작함
    return a+b;
};

console.log(sum_again(10,20));

console.log("---Answer");

/// 변환 문제 ///

function hello_basic(){
    console.log("hello");
    console.log("world");
}
hello_basic()

const hello_simple = () => {
    console.log("hello");
    console.log("world");
}
hello_simple()

console.log("---");

/// 변환 문제 ///

function sum_basic(a, b){
    return a + b
}
console.log(sum_basic(1,2));

const sum_simple = (a, b ) => a + b;
console.log(sum_simple(1,2));

console.log("---");

/// 변환 문제 ///

function greeting_basic(user) {
    console.log(`Hello, ${user}`);
}
greeting_basic('pkm')

const greeting_simple = (user) => console.log(`Hello, ${user}`);
greeting_simple('pkm');

const greeting_simple_2 = user => {
    console.log(`Hello, ${user}`);
}
greeting_simple_2('pkm');