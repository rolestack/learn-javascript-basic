// 오브젝트의 키값을 반복문으로 모두 출력
let obj = {
    x: 1,
    y: 2,
    z: 3
};

for (key in obj){
    console.log(key);
}

// 축약 표현(중복되는 이름을 한번만 선언)
const name = "최형배";
const country = "KR"

const user = {
    name,
    country
};

console.log(user);

// 오브젝트에 함수를 넣을 수 있다!
obj ={
    greeting: function () {
        console.log('Hi');
    },
};
obj.greeting()

// 축약도 가능하다!
obj ={
    greeting() {
        console.log('Hi');
    },
};
obj.greeting();

// 오브젝트의 객체드를 하나의 배열로 반환
obj = {
    x: 10,
    y: 20,
    z: 30,
}
console.log(Object.keys(obj)); // 오브젝트의 키를 배열(Array)로 반환
console.log(Object.values(obj)); // 오브젝트의 값을 배열(Array)로 반환
console.log(Object.entries(obj)); // 각각의 키값 쌍을 하나의 배열로 묶어서 개별적인 요소의 배열로 반환