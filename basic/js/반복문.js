// for
// for (시작; 조건; 증감;){ }
for (let i = 0; i <= 5; i++){
    console.log(i);
}

console.log("---");

let i2 = 0;
for (; i2 < 5; i2++){
    console.log(i2);
}

console.log("---");

let i3 = 0;
for (; i3 < 5;){
    console.log(i3++)
}

console.log("---");

for (let i = 0; i < 10; i++){
    if (i === 7){
        break;
    }
    console.log(i);
}

console.log("---");

for (let i = 0; i < 10; i++){
    if (i === 7){
        continue;
    }
    console.log(i);
}