// const title = document.getElementById('title');
let title = document.getElementsByClassName('title_class');
console.log(title);

let tag = document.getElementsByTagName('h2');
console.log(tag);

// 쿼리 셀렉터는 html과 css모두 선택 가능
// const query_selector_html = document.querySelector('h2'); // html
const query_selector_html = document.querySelector('#title'); // html 태그는 전부 다됨
console.log(query_selector_html);
const query_selector_css = document.querySelector('.title_class'); //css는 클래스 기준
console.log(query_selector_css);

// 쿼리 셀럭터 올은 인자 지정한 다수의 요소를 배열처럼 가져오기위해 사용
const quest_selector_all = document.querySelectorAll('.title_class');

title = document.querySelector('h2');
title.innerText = '안녕하세요!';
title.style.color = 'blue';

title = document.createElement('h1');
let body = document.querySelector('body');
title.innerText = "새로운 친굽니다.";
title.style.color = "red";
body.appendChild(title)