// 여러 값을 내보내는데 유용

// 이름이 정의된 export
export function named_1() {
    console.log('named_1');
};

export const named_2 = () => {
    console.log('named_2');
};

export const named_3 = () => {
    console.log('named_3');
};