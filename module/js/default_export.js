// 모듈당 하나의 export만 가능
const default_export = () => {
    console.log('default_export');
};

export default default_export; // ()는 없애고 export

export const named_export = () => {
    console.log('name_export');
};