// Named Export - import
import { named_1, named_2 } from './named_export.js'; // named export type 1
import * as main_component from './named_export.js'; // named export type 2

// Default Export - import
import default_function from './default_export.js';
// Named Export - import
import {named_export} from './default_export.js';

named_1(); // named export type 1
named_2(); // named export type 1
console.log("named export 1 done");
main_component.named_1(); // named export type 2
main_component.named_2(); // named export type 2
main_component.named_3(); // named export type 2
console.log("named export 2 done");

default_function(); // default export
named_export(); // named export in default_export.js file
console.log("default export done")